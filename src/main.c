#include"cbuffer.h"
#define Write_Data
//#define Read_Data
#define Count_Space 
//#define Clear_Buffer
#define Count_Data
int main()
{
cbuffer_t cb;
uint8_t cb_buff[8];
cb_init(&cb, cb_buff, 8);
#ifdef Write_Data
    uint8_t a[5] = { 1, 1, 3, 3, 4};
    cb_write(&cb, a, 5);
   // uint8_t e[5] = { 1, 2, 3, 4, 5};
    //cb_write(&cb,e,5);

#endif
#ifdef Read_Data 

    uint8_t b[3] = {0,0,0};
    cb_read(&cb, b, 3);
    printf("\nData Reader: ");
    for(uint8_t i=0; i< 3;i++)
    {   
        printf(" %d ",b[i]);
    }
   uint8_t c[2] = {0,0};
    cb_read(&cb, c, 2);
   printf("\nData Reader: ");
    for(uint8_t i=0; i< 2;i++)
   {   
        printf(" %d ",c[i]);
   }
 #endif   
    //Space count 
#ifdef Count_Space 
    uint8_t cb_space_index = cb_space_count(&cb);
    printf("\nSpace Count: %d",cb_space_index);
  //  cb_write(&cb, a, 5);
#endif
    //Clear Buffer
#ifdef Clear_Buffer
    cb_clear(&cb);  
    //printf("%d %d", cb.reader, cb.writer);  
#endif 
#ifdef Count_Data 
    //Count data
    uint8_t cb_count_index = cb_data_count(&cb);
    printf("\nData Count: %d",cb_count_index);
#endif 

}