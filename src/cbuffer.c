#include"cbuffer.h"

static uint8_t cb_w = 0;
static uint8_t cb_r = 0;
//static uint8_t overflow = 0;
static uint8_t m = 0;
static uint8_t count_data_overflow= 0;
void cb_init(cbuffer_t *cb, void *buf, uint32_t size)
{
    cb->data = (uint8_t*)buf;
    cb->active = true;
    cb->writer = 0;
    cb->reader = 0;
    cb->overflow = 0;
    cb->size = size;

}
void cb_clear(cbuffer_t *cb)
{
    for (uint8_t i=0; i<cb->writer;i++)
    {
        cb->data[i]=0;
    }
    cb->writer = 0;
    cb->reader = 0;
    cb->overflow = 0;
    cb_w = cb->writer;
    cb_r = cb->reader;   
}
uint32_t cb_read(cbuffer_t *cb, void *buf, uint32_t nbytes)
{
   // printf("\nData reader: ");
    if(cb->overflow==1)
        {
            cb->reader = cb->writer;
            cb->overflow = 0;
        }
    for (uint8_t i = 0;i < nbytes;i++)
    {      
        *((uint8_t*)buf+i)= cb->data[cb->reader];
        cb->reader++;
        cb_r++;

      //  printf(" %d ",cb->data[cb->reader]);
    }
    //cb_r = cb->reader;
    return cb->reader;
}
uint32_t cb_write(cbuffer_t *cb, void *buf, uint32_t nbytes)
{     
   printf("\nData writer: ");
   for (uint8_t i = 0;i < nbytes;i++)
    {   
        if (cb->writer == cb->size)
        {
           cb->writer = 0;
        } 
        count_data_overflow  == 0;
        cb->data[cb->writer] = *((uint8_t*)buf+i);       
        printf(" %d ", cb->data[cb->writer]);
        cb->writer++;
        cb_w++;           
    }
    if (cb_w >= cb->size + cb_r + 1)
    {
        cb->overflow = 1;
        count_data_overflow = 1;
        printf("\nCbuffer overflows: ");
        printf("%d bytes", cb_w - cb->size - cb_r);
    } 
    //cb_w=cb->writer;
    return cb->writer;
}
uint32_t cb_data_count(cbuffer_t *cb)
{
    
    if ( count_data_overflow  == 0 )
    {
        return cb->writer - cb->reader;        
    }                  
    if (count_data_overflow ==1)
    {
      if (cb->reader == 0) {return cb->size;} 
      else {return cb->size + cb->writer - cb->reader;}
    }     
}
uint32_t cb_space_count(cbuffer_t *cb)
{
    if ( count_data_overflow==0 )
    {return cb->size - cb->writer + cb->reader ;}
    else {
    if (cb->reader == 0) return 0;
    else return cb->reader - cb->writer;    
    } 
}